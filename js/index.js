$('document').ready(function(){ 
    var pagina=$('.page-item + .active').text();  //Referecia a las 2 clases simultáneas. También se podría generar una id...
    
    $.ajax({
        method:'post',
        url: 'php/creando_registros.php',
        data: {'pagina': pagina}
    })
    .done(function(respuesta){
        $('#ocurrencias').html(respuesta);
    })
    .fail(function(mensaje){
        alert('Error '+mensaje);
    })
});
