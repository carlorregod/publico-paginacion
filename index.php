<?php
//Preparaciones:
require_once 'php/conexion.php';
$pgsql=conexionBD();
$query='SELECT nombre FROM agenda_paginacion';
$consulta=$pgsql->prepare($query);
$consulta->execute();
//Calculando parámetros de paginacion
$filas=$consulta->rowCount();
$muestras_x_pagina=5;
$pagina=ceil($filas/$muestras_x_pagina);

?>

<!DOCTYPE html>
<html lang="es">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <!-- Estilos propios anexos -->
    <link rel="stylesheet" type="text/css" href="css/index.css">

    <title>Paginación</title>
  </head>
  <body>
    <h1 class="mb-5 my-3 mx-5 text-success">Paginando datos</h1>
    
    <div id="ocurrencias"></div>

    <nav aria-label="Page navigation example">
        <ul class="pagination justify-content-end">
            <li class="page-item <?php echo $_GET['pagina']<=1 ? 'disabled': ''?>"><a class="page-link" href="index.php?pagina=<?php echo $_GET['pagina']-1 ?>">Anterior</a></li>
            <?php if(!isset($_GET['pagina'])): ?>
              <li class="page-item active"><a class="page-link" href="index.php?pagina=1">1</a></li>
              <?php for($i=2;$i<=$pagina;$i++): ?>
              <li class="page-item"><a class="page-link" href="index.php?pagina=<?php echo $i ?>"><?php echo $i ?></a></li>
              <?php endfor ?>
            <?php else: ?>
              <?php for($i=1;$i<=$pagina;$i++): ?>
              <li class="page-item <?php echo $_GET['pagina']==$i ? 'active': ''?>"><a class="page-link" href="index.php?pagina=<?php echo $i ?>"><?php echo $i ?></a></li>
              <?php endfor ?>
            <?php endif ?>
            <li class="page-item <?php echo $_GET['pagina']>=$pagina ? 'disabled': ''?>"><a class="page-link" href="index.php?pagina=<?php echo $_GET['pagina']+1 ?>">Siguiente</a></li>
        </ul>
    </nav>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <!--Scripts propios -->
    <script type="text/javascript" src="js/index.js"></script>
  </body>
</html>
